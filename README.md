# Twitter Producer for Kafka 0.9 API
This project has used as reference this github:
https://github.com/mapr-demos/kafka-sample-programs

## Compile the jar and Build the image
```
$ mvn clean package docker:build
...
```

## Environment Variables:
```
bootstrap.servers = Location of the kafka bootstrap {kafka:29092}
number.iterations = Number of iterations {if 0 is infinite iterations} 
consumerKey = Consumer key for Twitter API
consumerSecret = Consumer Secret for Twitter API
token = Token for Twitter API
secret = Secret for Twitter API
hashtags = Hashtag that will be recoverer for api twetter {#NBAFinals,#TuesdayMotivation}
```