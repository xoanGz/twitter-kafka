FROM java:8-jre 
MAINTAINER dataspartan 
ADD ./target/twitter-1.0-SNAPSHOT-jar-with-dependencies.jar / 
CMD ["sh", "-c", "java -jar twitter-1.0-SNAPSHOT-jar-with-dependencies.jar"]
