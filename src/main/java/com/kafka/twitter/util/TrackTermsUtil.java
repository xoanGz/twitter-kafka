package com.kafka.twitter.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.kafka.twitter.model.TrackTermsVO;

public class TrackTermsUtil {
	
	final static Logger logger = Logger.getLogger(TrackTermsUtil.class);
	
	public static final String HASH_TAGS = "hashtags";
	public static List<String> trackTerms = null;
	
	/**
	 * Load the track terms for twitter form the env
	 * 
	 * @return
	 * @throws IOException
	 */
	public static List<String> loadTrackTerms() {
		if (trackTerms == null) {
			logger.info("Entering loadTrackTerms()");
			TrackTermsVO trackTermsVO = TrackTermsUtil.loadFromEnv();
			String hashtags = trackTermsVO.getHashTags();
			trackTerms = Arrays.asList(hashtags.split(","));
		}
		return trackTerms;
	}
	
	/**
	 * Load vo from env var
	 * @return
	 */
	private static TrackTermsVO loadFromEnv() {
		TrackTermsVO result = new TrackTermsVO();
		result.setHashTags(SystemUtil.getEnv(HASH_TAGS, 
				"#MITB,#Nabra,#MySelfDeprecation,#CharleenaLyles,#americangods"));
		return result;
	}
	
}
