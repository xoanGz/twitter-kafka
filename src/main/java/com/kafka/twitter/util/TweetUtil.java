package com.kafka.twitter.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kafka.twitter.model.tweet.TweetEntityVO;
import com.kafka.twitter.model.tweet.TweetHashtagVO;
import com.kafka.twitter.model.tweet.TweetVO;

public class TweetUtil {

	final static Logger logger = Logger.getLogger(TweetUtil.class);

	public static Map<String, String> analyzeTweet(TweetVO tweet) {
		Map<String, String> result = new HashMap<String, String>();
		logger.debug("Analyzing tweet: " + tweet);
		List<String> trackTerms = TrackTermsUtil.loadTrackTerms();

		for (String term : trackTerms) {
			if (tweetContainsHashtag(tweet, term)) {
				result.put(term, tweet.getText());
			}
		}

		return result;
	}

	/**
	 * Return if the tweet contains the hastag
	 * 
	 * @param tweet
	 * @param hashtag
	 * @return
	 */
	private static boolean tweetContainsHashtag(TweetVO tweet, String hashtag) {
		TweetEntityVO entities = tweet.getEntities();
		if (entities != null && entities.getHashtags() != null && !entities.getHashtags().isEmpty()) {
			for (TweetHashtagVO value : entities.getHashtags()) {
				String searchTerm = hashtag.replace("#", "");
				if (searchTerm.equalsIgnoreCase(value.getText())) {
					return true;
				}
			}
		}
		return false;
	}

}
