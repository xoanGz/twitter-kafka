package com.kafka.twitter.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.kafka.twitter.model.AccountDetailsTwitterVO;

public class AccountDetailsTwitterUtil {
	
	final static Logger logger = Logger.getLogger(AccountDetailsTwitterUtil.class);
	
	public static final String CONSUMER_KEY = "consumerKey";
	public static final String CONSUMER_SECRET = "consumerSecret";
	public static final String TOKEN = "token";
	public static final String SECRET = "secret";
	
	/**
	 * Load twitter data from env
	 * @return
	 * @throws IOException
	 */
	public static Map<String, String> getTwitterAcountDetails() throws IOException {
		logger.info("Entering loadTwitterAcountDetails()");
		Map<String, String> details = new HashMap<String, String>();
		AccountDetailsTwitterVO detailsVO = AccountDetailsTwitterUtil.loadFromEnv();
		details.put(AccountDetailsTwitterUtil.CONSUMER_KEY, detailsVO.getConsumerKey());
		details.put(AccountDetailsTwitterUtil.CONSUMER_SECRET, detailsVO.getConsumerSecret());
		details.put(AccountDetailsTwitterUtil.TOKEN, detailsVO.getToken());
		details.put(AccountDetailsTwitterUtil.SECRET, detailsVO.getSecret());
		return details;
	}
	
	/**
	 * Load vo from env var
	 * @return
	 */
	private static AccountDetailsTwitterVO loadFromEnv() {
		AccountDetailsTwitterVO result = new AccountDetailsTwitterVO();
		result.setConsumerKey(SystemUtil.getEnv(CONSUMER_KEY, "4yRsu5MSDSmHl39hlVa4D2s63"));
		result.setConsumerSecret(SystemUtil.getEnv(CONSUMER_SECRET, "9eBFaNpPj7BlGnCsHX6jwENePQxf3bodJWwsUbqJBbsT5cXvsr"));
		result.setToken(SystemUtil.getEnv(TOKEN, "39140523-naBITnGnhaGqs3uJhQPYej3eFhFcHq1nNfdCLFiWj"));
		result.setSecret(SystemUtil.getEnv(SECRET, "noWSy3r0fJ0OJEsEjHumNXjCFjsFzuc2K2EPh7uiTb0em"));
		return result;
	}
	
}
