package com.kafka.twitter.util;

import org.apache.log4j.Logger;

public class SystemUtil {
	
	final static Logger logger = Logger.getLogger(SystemUtil.class);
	private static boolean debug = Boolean.TRUE;
	

	/**
	 * Get value form system environment
	 * @param key
	 * @return
	 */
	public static String getEnv(String key) {
		return getEnv(key, null);
	}
	
	/**
	 * Get value form system environment
	 * with default value
	 * 
	 * @param key
	 * @return
	 */
	public static String getEnv(String key, String def) {
		String result = null;
		if (debug) {
			logger.info("Getting env var:" + key);
		}
		result = System.getenv(key);
		if (result==null) result = def;
		
		if (debug) {
			logger.info("Env var:" + key + " has value " + result);
		}
		return result;
	}
}
