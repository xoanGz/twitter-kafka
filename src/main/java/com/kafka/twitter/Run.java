package com.kafka.twitter;

import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * Pick whether we want to run as producer or consumer. This lets us have a
 * single executable as a build target.
 */
public class Run {
	
	final static Logger logger = Logger.getLogger(Run.class);
	
	public static void main(String[] args) throws IOException {
		try {
			logger.info("Entering the Kafka Twitter Producer");
			TwitterProducer.run(args);
		} catch (Exception e) {
			logger.error("Exiting the Kafka Twitter Producer with error", e);
		} finally {
			logger.info("Exiting the Kafka Twitter Producer");
		}
	}
}
