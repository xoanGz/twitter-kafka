package com.kafka.twitter.model.tweet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TweetHashtagVO {

	private String text;
	
	public TweetHashtagVO() { }

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "TweetHashtagVO [text=" + text + "]";
	}

}
