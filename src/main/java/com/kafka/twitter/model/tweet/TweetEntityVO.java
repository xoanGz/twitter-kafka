package com.kafka.twitter.model.tweet;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TweetEntityVO {

	private List<TweetHashtagVO> hashtags;
	
	public TweetEntityVO() { }

	public List<TweetHashtagVO> getHashtags() {
		return hashtags;
	}

	public void setHashtags(List<TweetHashtagVO> hashtags) {
		this.hashtags = hashtags;
	}

	@Override
	public String toString() {
		return "TweetEntityVO [hashtags=" + hashtags + "]";
	}
	
}
