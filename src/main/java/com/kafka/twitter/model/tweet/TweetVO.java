package com.kafka.twitter.model.tweet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TweetVO {

	private String text;
	private TweetEntityVO entities;
	
	public TweetVO() { }

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public TweetEntityVO getEntities() {
		return entities;
	}

	public void setEntities(TweetEntityVO entities) {
		this.entities = entities;
	}

	@Override
	public String toString() {
		return "TweetVO [text=" + text + ", entities=" + entities + "]";
	}
	
}
