package com.kafka.twitter.model;

public class TrackTermsVO {

	private String hashTags;
	
	public TrackTermsVO() { }
	
	public TrackTermsVO(String hashTags) {
		this.hashTags = hashTags;
	}
	
	public String getHashTags() {
		return hashTags;
	}

	public void setHashTags(String hashTags) {
		this.hashTags = hashTags;
	}

	@Override
	public String toString() {
		return "TrackTermsVO [hashTags=" + hashTags + "]";
	}
	
}
