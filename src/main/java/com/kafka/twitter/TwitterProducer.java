package com.kafka.twitter;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import com.kafka.twitter.model.tweet.TweetVO;
import com.kafka.twitter.util.AccountDetailsTwitterUtil;
import com.kafka.twitter.util.SystemUtil;
import com.kafka.twitter.util.TrackTermsUtil;
import com.kafka.twitter.util.TweetUtil;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

/**
 * 
 * This class will get a bunch of twitters comments by a hashtag that will be
 * sent to a kafka queue
 * 
 */
public class TwitterProducer {

	final static Logger logger = Logger.getLogger(TwitterProducer.class);
	private static final String topic = "twitter";
	private static final ObjectMapper mapper = new ObjectMapper();

	/**
	 * Run the application
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void run(String[] args) throws IOException {

		// Create the producer
		KafkaProducer<String, String> producer = TwitterProducer.createProducer();

		// Load the hashtags track terms for the Twitter Api
		List<String> trackTerms = TrackTermsUtil.loadTrackTerms();

		// Create Queue for Twitter
		BlockingQueue<String> queue = new LinkedBlockingQueue<String>(10000);
		
		// Get the Twitter account details
		Map<String, String> accountDetails = AccountDetailsTwitterUtil.getTwitterAcountDetails();

		// Create twitter Client
		Client client = TwitterProducer.createTwitterClient(accountDetails, trackTerms, queue);

		/**
		 * 
		 * Receive Twitter Messages and send it to kafka
		 * 
		 */

		// Establish a connection
		client.connect();

		Integer numIterations = Integer.parseInt(SystemUtil.getEnv("number.iterations", "10"));
		int msgRead = 0;
		logger.info("Starting sending to kafka with number of iterations (0 equals infinite) :  " + numIterations);
		if (numIterations > 0) {
			for (msgRead = 0; msgRead < numIterations; msgRead++) {
				sendTweetToKafka(producer, queue);
				if (msgRead % 20 == 0) logger.info("Sent msg number: " + msgRead);
			}
		} else {
			while(true) {
				sendTweetToKafka(producer, queue);
				if (msgRead % 20 == 0) logger.info("Sent msg number: " + ++msgRead);
			}
		}

		producer.close();
		client.stop();

	}

	/**
	 * Send tweets to kafka
	 * Note: The API returns if hashtag appear in the JSON.
	 * It can appear but becouse it is a quoted tweet
	 * Those are not send to kafka
	 * 
	 * @param producer
	 * @param queue
	 * @throws IOException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 */
	private static void sendTweetToKafka(KafkaProducer<String, String> producer, BlockingQueue<String> queue)
			throws IOException, JsonParseException, JsonMappingException {
		try {
			String rawtweet = queue.take();
			TweetVO tweetVO = mapper.readValue(queue.take(), TweetVO.class);
			Map<String, String> analyzeTweet = TweetUtil.analyzeTweet(tweetVO);
			if (!analyzeTweet.isEmpty()) {
				analyzeTweet.forEach((k, v) -> producer.send(new ProducerRecord<String, String>(topic, k, rawtweet)));
				logger.info("Sent to the queue");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the kafka produced from producer.props file
	 * 
	 * @return KafkaProducer
	 * @throws Exception
	 */
	private static KafkaProducer<String, String> createProducer() throws IOException {
		logger.info("Entering createProducer()");
		try {
			KafkaProducer<String, String> producer;
			InputStream props = Resources.getResource("producer.props").openStream();
			Properties properties = new Properties();
			properties.load(props);
			properties.put("bootstrap.servers", SystemUtil.getEnv("bootstrap.servers", "localhost:9092"));
			producer = new KafkaProducer<>(properties);
			return producer;
		} catch (IOException e) {
			logger.error("Error in createProducer.", e);
			throw e;
		}
	}

	/**
	 * Create the Twitter Client
	 * 
	 * @param queue
	 * @param trackTerms
	 * @return
	 */
	private static Client createTwitterClient(Map<String, String> accountDetails, List<String> trackTerms,
			BlockingQueue<String> queue) {
		logger.info("Entering createTwitterClient()");

		StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();
		endpoint.trackTerms(trackTerms);
		Authentication auth = new OAuth1(accountDetails.get("consumerKey"), accountDetails.get("consumerSecret"),
				accountDetails.get("token"), accountDetails.get("secret"));

		Client client = new ClientBuilder().hosts(Constants.STREAM_HOST).endpoint(endpoint).authentication(auth)
				.processor(new StringDelimitedProcessor(queue)).build();
		return client;
	}

}
