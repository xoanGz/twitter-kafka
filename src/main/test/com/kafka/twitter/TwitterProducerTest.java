package com.kafka.twitter;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.apache.kafka.clients.producer.MockProducer;
import org.junit.Rule;
import org.junit.Test;

import info.batey.kafka.unit.KafkaUnitRule;
import kafka.producer.KeyedMessage;

public class TwitterProducerTest {

	MockProducer<String, String> producer;

//	@Rule
	public KafkaUnitRule kafkaUnitRule = new KafkaUnitRule();

//	@Test
	public void testProducer() throws IOException {
		try {
			String testTopic = "TestTopic";
			kafkaUnitRule.getKafkaUnit().createTopic(testTopic);
			KeyedMessage<String, String> keyedMessage = new KeyedMessage<>(testTopic, "key", "value");
			
			kafkaUnitRule.getKafkaUnit().sendMessages(keyedMessage);
			List<String> messages;
			messages = kafkaUnitRule.getKafkaUnit().readMessages(testTopic, 1);
			assertEquals(Arrays.asList("value"), messages);
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
